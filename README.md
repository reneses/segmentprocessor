# README #

### Objective ###
This program extracts the segments from the SDLXLIFF and XLF files and output them in lines, avoiding the complexity of the XML view.

### How to work ###
1. Put the JAR file in the same directory than the SDLXLIFF and XLF files
2. Execute the program by means of the command line supplying the arguments:
    - *<basename>*: Basename of the files. Example: if they are called **MyFile.sdlxliff** and **MyFile.xlf**, the base name will be **MyFile**
    - *-differences*: Remove the segments which are present in both files
    - *-noblanks*: Filter the blank segments
    - *-sorted*: Sort the segments
    - *-console*: Print the output in the console
    - *-text*: Output the result as a text file
    - *-csv*: Output the result as a table in a CSV file