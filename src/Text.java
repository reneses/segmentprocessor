/**
 * Class containing the char and text constants
 */
public abstract class Text {

    public static final char NEW_LINE = '\n';
    public static final char SEPARATOR = '\t';

}
