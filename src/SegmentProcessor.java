import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Process SDLXLIFF and XLF segments, retrieving them from the XML structure
 *
 * TODO. Implement SimpleXML as XML parser
 * TODO. Solve segment leading spaces which cannot be trimmed
 * TODO. Implement option to process just one file with -sdl or -xlf options
 */
public class SegmentProcessor {

    /**
     * Complete constructor
     * @param basename Basename of the .sdlxliff and .xlf documents. Example: [basename].xlf & [basename].sdlxliff
     * @param options Processing options to execute
     */
    public void process(String basename, List<ProcessingOptions> options) {

        // Perform segmentation option
        boolean performSegmentation = ! options.contains(ProcessingOptions.DO_NOT_SEGMENT);

        // Obtain SDL segments
        String SDLFilename = basename + ".sdlxliff";
        System.out.println("*** PROCESSING " + SDLFilename + " ***");
        List<String> SDLSegments = getSegments(SDLFilename, performSegmentation);

        // Obtain OKAPI segments
        String OKAPIFilename = basename + ".xlf";
        System.out.println("*** PROCESSING " + OKAPIFilename + " ***");
        List<String> OKAPISegments = getSegments(OKAPIFilename, performSegmentation);

        // Print similitude
        // This has to happen before altering the list in the next options
        if (options.contains(ProcessingOptions.SIMILITUDE)) {
            printSimilitude(SDLSegments, OKAPISegments);
        }

        // Compare the segments
        if (options.contains(ProcessingOptions.REMOVE_IDENTICAL)) {
            System.out.println("*** REMOVING DUPLICATES ***");
            removeIdentical(SDLSegments, OKAPISegments);
        }

        // Omit blank
        if (options.contains(ProcessingOptions.NO_BLANK)) {
            System.out.println("*** REMOVING BLANK SEGMENTS ***");
            removeBlank(SDLSegments, OKAPISegments);
        }

        // Sort
        if (options.contains(ProcessingOptions.SORT)) {
            System.out.println("*** SORTING SEGMENTS ***");
            sortSegments(SDLSegments, OKAPISegments);
        }

        // Export in linear string (no columns)
        if (options.contains(ProcessingOptions.TO_TEXT)) {

            // SDL
            if (SDLSegments.isEmpty()) {
                System.err.println("(!) There are not SDL segments to be outputted");
            }
            else {
                System.out.println("*** EXPORTING TO " + basename + "_SDL.out ***");
                String SDLoutput = toLinearString(SDLSegments);
                exportToFile(basename + "_SDL.out", SDLoutput);
            }

            // OKAPI
            if (OKAPISegments.isEmpty()) {
                System.err.println("(!) There are not OKAPI segments to be outputted");
            }
            else {
                System.out.println("*** EXPORTING TO " + basename + "_OKAPI.out ***");
                String OKAPIoutput = toLinearString(OKAPISegments);
                exportToFile(basename + "_OKAPI.out", OKAPIoutput);
            }

        }

        // Export in linear string (no columns)
        if (options.contains(ProcessingOptions.TO_CSV)) {
            System.out.println("*** EXPORTING TO " + basename + ".out.csv ***");
            String columnsOutput = toColumnsString(SDLSegments, OKAPISegments);
            exportToFile(basename + ".out.csv", columnsOutput);
        }

        // Print to console
        if (options.contains(ProcessingOptions.PRINT)  ||
                !options.contains(ProcessingOptions.SIMILITUDE) &&
                        !options.contains(ProcessingOptions.TO_TEXT)  &&
                        !options.contains(ProcessingOptions.TO_CSV)) {

            // SDL
            if (!SDLSegments.isEmpty()) {
                System.out.println("*** PRINTING SDL ***");
                System.out.println(toLinearString(SDLSegments));
            }

            // OKAPI
            if (!SDLSegments.isEmpty()) {
                System.out.println("\n*** PRINTING OKAPI ***");
                System.out.println(toLinearString(OKAPISegments));
            }

        }

        // Add one more space
        System.out.println();

    }


    /**
     * Get the segments contained in one XLF file (regardless if it is XLF or SDLXLIFF)
     * @param filename XLF filename
     * @return List with the segments
     */
    private List<String> getSegments(String filename, boolean performSegmentation) {

        // Try to open the XML and retrieve the root
        Node root;
        try {
            root = getXMLRoot(filename);
        }
        catch (FileNotFoundException e) {
            System.err.println("(!) The file "+filename+" does not exist in the current directory");
            return new ArrayList<>();
        }
        catch (IOException e) {
            System.err.println("(!) The file "+filename+" cannot be processed");
            return new ArrayList<>();
        } catch (SAXException e) {
            System.err.println("(!) The file "+filename+" is not a correct XML document");
            return new ArrayList<>();
        }

        // Output list
        List<String> segments = new LinkedList<>();

        // Obtain the list of files and process them
        List<Node> fileList = XMLHelper.getNodes("file", root.getChildNodes());
        for (Node file : fileList)
            segments.addAll(processFile(file, performSegmentation));

        // Check if they are empty
        if (segments.isEmpty())
            System.err.println("(!) The file "+filename+" does not contain any segment");

        // Return segments
        return segments;

    }


    /**
     * Open the XML file and retrieve the root node
     * @param filename XML filename
     * @return root node
     * @throws IOException
     * @throws SAXException
     */
    private Node getXMLRoot(String filename) throws IOException, SAXException {

        // Prepare the parser
        DOMParser parser = new DOMParser();
        parser.parse(filename);
        Document doc = parser.getDocument();

        // Get the document's root XML node
        Node root = XMLHelper.getNode("xliff", doc.getChildNodes());
        assert root != null;

        // Return root
        return root;
    }


    /**
     * Process a file tag to obtain the segments contained inside it
     * @param file File node
     * @return List with the segments
     */
    private List<String> processFile(Node file, boolean performSegmentation) {

        // Output list
        List<String> segments = new LinkedList<>();

        // Obtain the boddy
        Node body = XMLHelper.getNode("body", file.getChildNodes());
        assert body != null;

        // Obtain non-grouped trans-units
        List<Node> transUnitList = XMLHelper.getNodes("trans-unit", body.getChildNodes());

        // Obtain grouped trans-units
        XMLHelper.getNodes("group", body.getChildNodes()).forEach(
                group -> transUnitList.addAll(XMLHelper.getNodes("trans-unit", group.getChildNodes()))
        );

        // Process the trans-units
        for (Node unit : transUnitList)
            segments.addAll(processUnit(unit, performSegmentation));

        // Return the segments
        return segments;
    }


    /**
     * Process a unit tag to obtain the segments contained inside it
     * @param unit Unit node
     * @return List with the segments
     */
    private List<String> processUnit(Node unit, boolean performSegmentation) {

        //System.out.println("** PROCESSING UNIT: "+XMLHelper.getNodeAttr("id",unit));

        // Create output
        List<String> segments = new ArrayList<>();

        // The units marked as "no translatable" by SDL are not taken into account (jut formatting info)
        if (XMLHelper.getNodeAttr("translate", unit).equals("no"))
            return segments;

        // If we are not segment, just obtain the unit (segments-pre split)
        if (!performSegmentation) {
            String segment = XMLHelper.getNodeValue("source", unit.getChildNodes());
            if (!segment.trim().equals(""))
                segments.add(segment);
        }

        // Otherwise, segment them
        else {

            // Obtain segments from direct units
            Node segSource = XMLHelper.getNode("seg-source", unit.getChildNodes());

            // If its a real source, add its segments
            if (segSource != null) {

                // Obtain MRK nodes
                List<Node> mrkList = XMLHelper.getNodes("mrk", segSource.getChildNodes());
                List<Node> gNodes = XMLHelper.getNodes("g", segSource.getChildNodes());

                // Go to all the gNodes
                for (Node gNode : gNodes) {

                    // Add direct MRK
                    mrkList.addAll(XMLHelper.getNodes("mrk", gNode.getChildNodes()));

                    // Go to the gNodes inside it
                    List<Node> gSubNodes = XMLHelper.getNodes("g", gNode.getChildNodes());

                    // Traverse this sublist
                    for (Node gSubNode : gSubNodes)
                        mrkList.addAll(XMLHelper.getNodes("mrk", gSubNode.getChildNodes()));
                }

                // SDL produces empty units, if this is the case, export an empty segment
                if (mrkList.size() == 0)
                    return Stream.of("").collect(Collectors.toList());

                // Process the nodes
                for (Node mrk : mrkList) {
                    String segment = mrk.getTextContent();
                    if (!segment.trim().equals(""))
                        segments.add(segment
                                .replace(String.valueOf(Text.NEW_LINE), "")
                                .replace(String.valueOf(Text.SEPARATOR), " ") // TODO solve the $#*@$� space which is not trimmed in JP docs
                                .trim());
                }
            }
        }

        // Return the result
        return segments;
    }


    /**
     * Process the SDL and OKAPI segments, removing the ones which are present in both lists
     * @param SDLSegments segments generated from the SDLXLIFF
     * @param OKAPISegments segments generated from the XLF
     */
    private void removeIdentical(List<String> SDLSegments, List<String> OKAPISegments) {

        // Copy SDL Segments list
        List<String> SDLSegmentsCopy = new ArrayList<>(SDLSegments);

        // SDL - Okapi
        SDLSegments.removeAll(OKAPISegments);

        // Okapi - SDL
        OKAPISegments.removeAll(SDLSegmentsCopy);

    }


    /**
     * Process the SDL and OKAPI segments, sorting both lists
     * @param SDLSegments segments generated from the SDLXLIFF
     * @param OKAPISegments segments generated from the XLF
     */
    private void sortSegments(List<String> SDLSegments, List<String> OKAPISegments) {

        // Sort lists
        Collections.sort(SDLSegments);
        Collections.sort(OKAPISegments);

    }

    /**
     * Process the SDL and OKAPI segments, removing empty segments
     * @param SDLSegments segments generated from the SDLXLIFF
     * @param OKAPISegments segments generated from the XLF
     */
    private void removeBlank(List<String> SDLSegments, List<String> OKAPISegments) {

        // Remove blanks
        SDLSegments.removeIf( segment -> segment.trim().equals(""));
        OKAPISegments.removeIf( segment -> segment.trim().equals(""));

    }



    /**
     * Compute the similitude percentage
     * @param SDLSegments segments generated from the SDLXLIFF
     * @param OKAPISegments segments generated from the XLF
     * @return Percentage of similitude in format [0,1]
     */
    private double computeSimilitude(List<String> SDLSegments, List<String> OKAPISegments) {

        // Copy the list
        List<String> SDLcopy = new ArrayList<>(SDLSegments);
        List<String> OKAPIcopy = new ArrayList<>(OKAPISegments);

        // Prepare them
        removeIdentical(SDLcopy, OKAPIcopy);
        removeBlank(SDLcopy, OKAPIcopy);

        // Return percentage
        return (double) (OKAPISegments.size() - OKAPIcopy.size()) / (double) OKAPISegments.size();

    }

    /**
     * Print the similitude percentage
     * @param SDLSegments segments generated from the SDLXLIFF
     * @param OKAPISegments segments generated from the XLF
     */
    private void printSimilitude(List<String> SDLSegments, List<String> OKAPISegments) {

        // Check that both lists have been created
        if (OKAPISegments.isEmpty()  ||  SDLSegments.isEmpty()) {
            System.err.println("(!) The similitude can only be compared over both files");
            return;
        }

        DecimalFormat df = new DecimalFormat("#.00");
        System.out.print("*** COMPUTING APPROX. SIMILITUDE:  ");
        String percentage = df.format(computeSimilitude(SDLSegments, OKAPISegments) * 100);
        System.out.printf("%s%% ***\n", percentage);

    }


    /**
     * Output the segments list in a sequential way
     * @param segments segments to be exported
     *  @return String containing the SDL's segments, followed by the OKAPI's ones
     */
    public String toLinearString(List<String> segments) {

        // Prepare output
        StringBuilder sb = new StringBuilder();

        // Export segments, one per line
        if (segments.size() > 0)
            segments.forEach(segment -> sb.append(segment).append(Text.NEW_LINE));

        // Return output
        return sb.toString();
    }


    /**
     * Output the segments list in two-columns table, stored in a CSV file with '\t' separator
     * @param SDLSegments segments generated from the SDLXLIFF
     * @param OKAPISegments segments generated from the XLF
     * @return String containing the SDL's segments, followed by the OKAPI's ones
     */
    public String toColumnsString(List<String> SDLSegments, List<String> OKAPISegments) {

        // Prepare output
        StringBuilder sb = new StringBuilder();
        sb.append("SDL").append(Text.SEPARATOR).append("OKAPI").append(Text.NEW_LINE);

        // Iterate through the part of same length
        int index = 0, until = Math.min(SDLSegments.size(), OKAPISegments.size());
        while (index < until) {

            // Load each segment
            String SDLSegment = SDLSegments.get(index);
            String OKAPISegment = OKAPISegments.get(index);

            // Append to output
            sb.append(SDLSegment).append(Text.SEPARATOR).append(OKAPISegment).append(Text.NEW_LINE);

            // Continue
            index++;
        }

        // Handle different lengths
        if (SDLSegments.size() > OKAPISegments.size())
            while (index < SDLSegments.size())
                sb.append(SDLSegments.get(index++)).append(Text.SEPARATOR).append("").append(Text.NEW_LINE);
        else if (SDLSegments.size() < OKAPISegments.size())
            while (index < OKAPISegments.size())
                sb.append("").append(Text.SEPARATOR).append(OKAPISegments.get(index++)).append(Text.NEW_LINE);

        // Return the output
        return sb.toString();
    }


    /**
     * Export content to a file
     * @param filename New file's name
     * @param content Content to be written into the new file
     */
    private void exportToFile(String filename, String content) {

        // Open the new file and write it
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(filename), "utf-8"))) {
            writer.write(content);
        } catch (IOException e) {
            System.err.print("It has not been possible to create/overwrite the '"+filename+"' file");
        }
    }

}