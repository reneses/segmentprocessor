import java.util.*;
import java.util.stream.Collectors;

/**
 * Main class which takes care of the execution of the code
 */
public class Main {

    // Input argument constants
    private static final String ARG_CSV = "-csv";
    private static final String ARG_TEXT = "-text";
    private static final String ARG_CONSOLE = "-console";
    private static final String ARG_DIFFERENCES = "-differences";
    private static final String ARG_NO_BLANK = "-noblank";
    private static final String ARG_SORTED = "-sorted";
    private static final String ARG_SIMILITUDE = "-similitude";
    private static final String ARG_DO_NOT_SEGMENT = "-units";

    /**
     * Main method
     * @param args arguments supplied
     */
    public static void main(String args[]) {

        // If we have arguments, execute them
        if (args.length > 0) {
            executeArguments(args);
        }

        // If not, show the possible arguments and give one more chance
        else {

            // Show instructions and give one more chance
            System.err.println("Required arguments: <basename>. Example: <basename>.sdlxliff & <basename>.xlif");
            System.err.println("Optional arguments: "
                    + ARG_DO_NOT_SEGMENT + ", " + ARG_SIMILITUDE + ", " + ARG_DIFFERENCES+", "+ARG_SORTED+", " + ARG_NO_BLANK
                    + ", " + ARG_CSV + ", " + ARG_TEXT + ", " + ARG_CONSOLE);

            // Ask again for the arguments
            System.out.print("Introduce arguments: ");
            String args2[] = new Scanner(System.in).nextLine().split(" ");

            // If we have arguments, execute them
            if (args2.length > 0)
                executeArguments(args2);

            // If not, show error and quit
            else
                System.err.println("Required parameters: <basename>");

        }
    }

    /**
     * Execute the arguments provided
     * @param args Arguments for the execution
     */
    private static void executeArguments(String args[]) {

        // Obtain the options
        Set<String> arguments = new LinkedHashSet<>(Arrays.asList(args));
        List<ProcessingOptions> options = retrieveOptions(arguments);

        // If the basename it is not specified, raise an error
        List<String> basenames = arguments.stream()
                .filter(argument -> !argument.startsWith("-"))
                .collect(Collectors.toList());
        if (basenames.isEmpty())
            System.err.println("Required parameter: <basename>");

        // If everything is correct, execute the processor
        else
            basenames.forEach( basename -> new SegmentProcessor().process(basename, options));

    }


    /**
     * Filter the input arguments, removing the options from the input
     * @param arguments Arguments for the execution
     * @return List containing the options
     */
    private static List<ProcessingOptions> retrieveOptions(Set<String> arguments) {

        // Process arguments
        List<ProcessingOptions> options = new ArrayList<>();
        if (arguments.contains(ARG_CSV))
            options.add(ProcessingOptions.TO_CSV);
        if (arguments.contains(ARG_TEXT))
            options.add(ProcessingOptions.TO_TEXT);
        if (arguments.contains(ARG_CONSOLE))
            options.add(ProcessingOptions.PRINT);
        if (arguments.contains(ARG_DIFFERENCES))
            options.add(ProcessingOptions.REMOVE_IDENTICAL);
        if (arguments.contains(ARG_SORTED))
            options.add(ProcessingOptions.SORT);
        if (arguments.contains(ARG_NO_BLANK))
            options.add(ProcessingOptions.NO_BLANK);
        if (arguments.contains(ARG_SIMILITUDE))
            options.add(ProcessingOptions.SIMILITUDE);
        if (arguments.contains(ARG_DO_NOT_SEGMENT))
            options.add(ProcessingOptions.DO_NOT_SEGMENT);

        // Clean list of arguments already processed
        arguments.removeAll(Arrays.asList(ARG_DO_NOT_SEGMENT, ARG_CSV, ARG_TEXT, ARG_CONSOLE,
                ARG_DIFFERENCES, ARG_NO_BLANK, ARG_SORTED, ARG_SIMILITUDE));

        // Check remaining options and remove them, showing errors
        new ArrayList<>(arguments).stream()
                .filter(argument -> argument.startsWith("-"))
                .forEach(option -> {
                    System.err.printf("(!) The argument %s couldn't be recognizedn\n", option);
                    arguments.remove(option);
                });

        // Return them
        return options;
    }

}
