/**
 * Processing options for the Segment Processor class
 */
public enum ProcessingOptions {

    SORT, NO_BLANK, REMOVE_IDENTICAL, PRINT, TO_CSV, SIMILITUDE, TO_TEXT, DO_NOT_SEGMENT

}
